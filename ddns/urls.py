from django.urls import path, include
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'hosts', HostViewSet)
router.register(r'records', RecordViewSet)
router.register(r'subscriptions', SubscriptionViewSet)


urlpatterns = [
    path('dnsupdate/subscription=<subscription>&hosts=<hosts>&ip=<ip>&secret=<secret>', DNSUpdate),
    path('acme_update/<secret>/present', ACME_present),
    path('acme_update/<secret>/cleanup', ACME_cleanup),
    path('', include(router.urls)),
]

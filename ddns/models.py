from django.db import models

__all__ = ['Subscription', 'Host', 'ARecord', 'TXTRecord']

class Subscription(models.Model):
    name = models.CharField('Domain of subscription', max_length=253)
    def __str__(self):
        return self.name

class Host(models.Model):
    host = models.CharField('Host for the record', max_length=253)
    sub = models.ForeignKey(Subscription, related_name='hosts', on_delete=models.CASCADE)
    def __str__(self):
        return self.host +'.'+ self.sub.name

class ARecord(models.Model):
    host = models.ForeignKey(Host, related_name='records', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    ip = models.CharField(max_length=45)
    time = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['-time']
    def __str__(self):
        return  self.ip + ' IN A ' + self.host.__str__()

class TXTRecord(models.Model):
    host = models.ForeignKey(Host, related_name='records_txt', on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    txt = models.CharField(max_length=45)
    time = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['-time']
    def __str__(self):
        return  self.txt + ' IN TXT ' + self.host.__str__()

from .models import *
from rest_framework import serializers

__all__ = ['SubscriptionSerializer', 'ARecordSerializer', 'HostSerializer']

class ARecordSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ARecord
        fields = ['id', 'host', 'active', 'ip', 'time']

class ARecordSetSerializer(serializers.ModelSerializer):
    class Meta:
        model = ARecord
        fields = ['id', 'active', 'ip', 'time']

class HostSerializer(serializers.HyperlinkedModelSerializer):
    records = ARecordSetSerializer(many=True, read_only=True)
    class Meta:
        model = Host
        fields = ['id','host','sub', 'records']

class HostSetSerializer(serializers.ModelSerializer):
    records = ARecordSetSerializer(many=True, read_only=True)
    class Meta:
        model = Host
        fields = ['id', 'host', 'records']

class SubscriptionSerializer(serializers.HyperlinkedModelSerializer):
    hosts = HostSetSerializer(many=True, read_only=True)
    class Meta:
        model = Subscription
        fields = '__all__'


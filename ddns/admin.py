from django.contrib import admin
from .models import *

class HostAdmin(admin.ModelAdmin):
    list_filter = (
        ('sub', admin.RelatedOnlyFieldListFilter),
    )

class ARecordAdmin(admin.ModelAdmin):
    list_display = ('time','host','ip','active')
    list_filter = (
        ('host', admin.RelatedOnlyFieldListFilter),
        ('host__sub', admin.RelatedOnlyFieldListFilter),
    )

class TXTRecordAdmin(admin.ModelAdmin):
    list_display = ('time','host','txt','active')
    list_filter = (
        ('host', admin.RelatedOnlyFieldListFilter),
        ('host__sub', admin.RelatedOnlyFieldListFilter),
    )

admin.site.register(Subscription)
admin.site.register(Host, HostAdmin)
admin.site.register(ARecord, ARecordAdmin)
admin.site.register(TXTRecord, TXTRecordAdmin)

import re
import httpx
from django.http import HttpResponse
from django.http.response import HttpResponseNotFound
from .models import *
from webutils.settings import ddns_APIKey, ddns_BaseURL


__all__ = [ 'addARecord', 'removeARecord', 'addTXTRecord', 'removeTXTRecord', 'getSubscription404', 'getHostname']

headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'X-API-Key': ddns_APIKey
}

def addARecord(rec: ARecord):
    ip = rec.ip
    subscription = rec.host.sub.name
    host = rec.host.host
    url = ddns_BaseURL + 'cli/dns/call'
    content = {
        "params": ['-a', subscription, '-a', host, '-ip', ip]
    }
    r = httpx.post(url=url, headers=headers, json=content)
    return True if r.status_code == 200 else False

def removeARecord(rec: ARecord):
    ip = rec.ip
    subscription = rec.host.sub.name
    host = rec.host.host
    url = ddns_BaseURL + 'cli/dns/call'
    content = {
        "params": ['-d', subscription, '-a', host, '-ip', ip]
    }
    r = httpx.post(url=url, headers=headers, json=content)
    return True if r.status_code == 200 else False

def removeTXTRecord(rec: TXTRecord):
    txt = rec.txt
    subscription = rec.host.sub.name
    host = rec.host.host
    url = ddns_BaseURL + 'cli/dns/call'
    content = {
        "params": ['-d', subscription, '-domain', "_acme-challenge." + host, '-txt', txt]
    }
    r = httpx.post(url=url, headers=headers, json=content)
    return True if r.status_code == 200 else False

def addTXTRecord(rec: TXTRecord):
    txt = rec.txt
    subscription = rec.host.sub.name
    host = rec.host.host
    url = ddns_BaseURL + 'cli/dns/call'
    content = {
        "params": ['-a', subscription, '-domain', "_acme-challenge." + host, '-txt', txt]
    }
    r = httpx.post(url=url, headers=headers, json=content)
    return True if r.status_code == 200 else False

def getSubscription404(acme_fqdn: str) -> Subscription:
    # Sanity check domain
    match = re.match('^_acme-challenge\.(?P<domain>[\w\._-]+)\.$', acme_fqdn)
    if match:
        domain = match.group('domain')
    else:
        return HttpResponse('Malformed FQDN', status=400)

    # Search for subscription beginning from TLD
    sub = None
    for i in domain.split('.'):
        try:
            sub = Subscription.objects.get(name=domain)
        except Subscription.DoesNotExist as e:
            domain = re.sub( i + '\.', '', domain)
            continue
        break
    if not sub:
        return HttpResponseNotFound('Subscription not found')
    return sub

def getHostname(acme_fqdn: str, sub: Subscription) -> str:
    hostname = re.sub( '\.' + sub.name + '\.', '', acme_fqdn)
    hostname = re.sub( '^_acme-challenge\.', '', hostname)
    return hostname

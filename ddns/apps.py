from django.apps import AppConfig


class DdnsConfig(AppConfig):
    name = 'ddns'

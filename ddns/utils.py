__all__ = ['validate_ip4']

def validate_ip4(ip: str):
    arr = ip.split('.')
    if len(arr) != 4:
        return False
    for x in arr:
        if not x.isdigit():
            return False
        i = int(x)
        if not (0<=i<=255):
            return False
    return True

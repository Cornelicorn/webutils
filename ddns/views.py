import re
import json
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, permissions
from rest_framework.decorators import api_view, permission_classes
from .serializers import *
from .models import *
from .plesk import *
from .utils import validate_ip4
from webutils.settings import ddns_secret


__all__ = ['SubscriptionViewSet', 'HostViewSet', 'RecordViewSet', 'DNSUpdate', 'ACME_present', 'ACME_cleanup']


class SubscriptionViewSet(viewsets.ModelViewSet):
    queryset = Subscription.objects.all()
    serializer_class = SubscriptionSerializer

class HostViewSet(viewsets.ModelViewSet):
    queryset = Host.objects.all()
    serializer_class = HostSerializer

class RecordViewSet(viewsets.ModelViewSet):
    queryset = ARecord.objects.all()
    serializer_class = ARecordSerializer


@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@csrf_exempt
def DNSUpdate(request, subscription, hosts, ip, secret):
    status_code = 200 if request.method == 'GET' else 201
    if not validate_ip4(ip):
        return HttpResponse('Malformed IP', status=400)
    if secret == ddns_secret:
        sub = get_object_or_404(Subscription, name=subscription)
        hosts = hosts.split(',')
        host_objects = [get_object_or_404(Host, sub=sub, host=host) for host in hosts]
        for host in host_objects:
            rec_exists = False
            records = host.records.filter(active=True)
            for rec in records:
                if rec.ip != ip:
                    removeARecord(rec)
                    rec.active = False
                    rec.clean()
                    rec.save()
                elif rec.ip == ip:
                    rec_exists = True
            if not rec_exists:
                rec = ARecord(host=host, ip=ip)
                addARecord(rec)
                rec.clean()
                rec.save()
        saved_ip = sub.hosts.get(host=hosts[0]).records.filter(active=True).first().ip
        return HttpResponse(f"{saved_ip}", status=status_code)
    else:
        return HttpResponse('Wrong secret', status=403)

@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
@csrf_exempt
def ACME_present(request, secret):
    data=json.loads(request.body)
    fqdn = data['fqdn']
    token = data['value']
    sub = getSubscription404(fqdn)
    if isinstance(sub, HttpResponse):
        return sub
    host = get_object_or_404(Host, sub=sub, host=getHostname(fqdn, sub))
    records = host.records_txt.filter(active=True)
    rec_exists = False
    for rec in records:
        if rec.txt != token:
            removeTXTRecord(rec)
            rec.active = False
            rec.save()
        elif rec.txt == token:
            rec_exists = True
    if not rec_exists:
        rec = TXTRecord(host=host, txt=token)
        addTXTRecord(rec)
        rec.save()
    return HttpResponse('success')


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
@csrf_exempt
def ACME_cleanup(request, secret):
    data=json.loads(request.body)
    fqdn = data['fqdn']
    token = data['value']
    sub = getSubscription404(fqdn)
    if isinstance(sub, HttpResponse):
        return sub
    host = get_object_or_404(Host, sub=sub, host=getHostname(fqdn, sub))
    records = host.records_txt.filter(active=True, txt=token)
    for rec in records:
        removeTXTRecord(rec)
        rec.active = False
        rec.save()
    return HttpResponse('success')


